﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using SignalRDataServer.Model;

namespace SignalRDataServer.Hubs
{
    public class DataSenderHub : Hub
    {
        IHubContext<DataSenderHub> _contxt;

        public DataSenderHub(IHubContext<DataSenderHub> context)
        {
            _contxt = context;
        }
        public override async Task OnConnectedAsync()
        {            
            await _contxt.Clients.All.SendAsync("Connected", Context.ConnectionId);
        }

        public async Task SendData(string groupId, OrderModel order)
        {
            await _contxt.Clients.Group(groupId).SendAsync("ReceiveData", order);
        }

        public async Task AddToGroup(string groupName)
        {
            await _contxt.Groups.AddToGroupAsync(Context.ConnectionId, groupName);
        }
    }
}