﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace SignalRDataServer.Pages
{
    [BindProperties]
    public class LogInModel : PageModel
    {
        private readonly SignInManager<IdentityUser> _signInManager;
        public LogInModel(SignInManager<IdentityUser> signMmanager)
        {
            _signInManager = signMmanager;
        }

        public void OnGet()
        {           
            
        }       

        public string UserName { get; set; }
        public string Password { get; set; }
        public async Task<IActionResult> OnPostAsync()
        {
            var signResult = await _signInManager.PasswordSignInAsync(UserName, Password, false, false);
            if (signResult.Succeeded)
                return RedirectToPage("PlaceOrder");
            return Page();
        }
    }
}