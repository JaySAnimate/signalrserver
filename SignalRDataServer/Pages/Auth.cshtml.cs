﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace SignalRDataServer.Pages
{
    public class AuthModel : PageModel
    {
        public async Task<IActionResult> OnGet()
        {
            List<Claim> UserCredentials = new List<Claim>()
                {
                    new Claim(ClaimTypes.Name, "John"),
                    new Claim(ClaimTypes.Role, "Admin")
                };

            ClaimsIdentity SignUpClaimIdentity = new ClaimsIdentity(UserCredentials, "CookieAuth");
            ClaimsPrincipal UserPrincipal = new ClaimsPrincipal(new[] { SignUpClaimIdentity });
            await HttpContext.SignInAsync(UserPrincipal);
            return RedirectToPage("PlaceOrder");
        }
    }
}