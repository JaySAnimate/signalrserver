﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.SignalR;
using SignalRDataServer.Hubs;
using SignalRDataServer.Model;

namespace SignalRDataServer.Pages
{  
    [Authorize]
    public class PlaceOrderModel : PageModel
    {
        public DataSenderHub _dataSender;

        [BindProperty]
        public OrderModel Order { get; set; }
        public PlaceOrderModel(IHubContext<DataSenderHub> dataSender)
        {

            _dataSender = new DataSenderHub(dataSender);
            Order = new OrderModel();
        }

        public IActionResult OnGet()
        {
            return Page();
        }        
        
        public async Task<IActionResult> OnPostAsync()
        {            
            await _dataSender.SendData("subscribers", Order);
            return Page();
        }
    }
}