﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace SignalRDataServer.Pages
{
    [BindProperties]
    public class SignUpModel : PageModel
    {
        private readonly UserManager<IdentityUser> _userManager;
        public SignUpModel(UserManager<IdentityUser> manager)
        {
            _userManager = manager;
        }

        public void OnGet()
        {

        }

        public string UserName { get; set; }
        public string Password { get; set; }
        public string Email { get; set; }
        public async Task<IActionResult> OnPostAsync()
        {
            var user = new IdentityUser
            {
                UserName = this.UserName,
                Email = this.Email
            };

            var createdUser = await _userManager.CreateAsync(user, Password);

            if (createdUser.Succeeded)
            {
                List<Claim> UserCredentials = new List<Claim>()
                {
                    new Claim(ClaimTypes.Name, "John"),
                    new Claim(ClaimTypes.Role, "Admin")
                };

                ClaimsIdentity SignUpClaimIdentity = new ClaimsIdentity(UserCredentials, "CookieAuth");
                ClaimsPrincipal UserPrincipal = new ClaimsPrincipal(new[] { SignUpClaimIdentity });
                await HttpContext.SignInAsync(UserPrincipal);

                return RedirectToPage("LogIn");
            }
            return Page();
        }
    }
}