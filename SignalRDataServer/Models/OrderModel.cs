﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SignalRDataServer.Model
{
    public class OrderModel
    {
        public int RestId{ get; set; }
        public int PersonCount { get; set; }
        public bool SmokingZone { get; set; }
        public bool VIPZone { get; set; }        
    }
}
