﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SignalRDataConsoleClient
{
    class Order
    {
        public int RestId { get; set; }
        public int PersonCount { get; set; }
        public bool SmokingZone { get; set; }
        public bool VIPZone { get; set; }
    }
}
