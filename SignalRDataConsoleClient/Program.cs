﻿using System;
using Microsoft.AspNetCore.SignalR.Client;

namespace SignalRDataConsoleClient
{
    class Program
    {
        static void Main()
        {
            HubConnection connection = new HubConnectionBuilder()
                .WithUrl("https://localhost:44303/dataSender")
                .Build();

            connection.On<Order>("ReceiveData", order => Console.WriteLine("Restaurant ID: {0}\n" +
                                                                            "Person Count: {1}\n" +
                                                                            "Somocking Zone: {2}\n" +
                                                                            "VIP Zone: {3}\n", order.RestId,
                                                                                               order.PersonCount,
                                                                                               order.SmokingZone,
                                                                                               order.VIPZone));

            connection.On<string>("Connected", id => Console.WriteLine("New Connection Id -> {0}", id));

            connection.StartAsync().GetAwaiter().GetResult();

            string subscribers = "subscribers";

            Console.WriteLine("Do you want to subscribe ? (y / n)");
            ConsoleKey key = Console.ReadKey().Key;
            if(key == ConsoleKey.Y)
            {
                connection.InvokeAsync("AddToGroup", subscribers).GetAwaiter().GetResult();
                Console.WriteLine("Subscribed!");
            }
            else 
                Console.WriteLine("You won't see any order");

            Console.WriteLine("Press Any Key To end Connection");
            Console.ReadKey();
        }
    }
}
